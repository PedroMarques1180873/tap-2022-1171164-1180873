import assessment.AssessmentBehaviours

class Assessment01 extends AssessmentBehaviours:
  val PATH = "files/assessment01files"   // Assessment file path
  performTests(assessment.AssessmentMS01.create, "Milestone 1")
