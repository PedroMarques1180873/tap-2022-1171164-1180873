import assessment.AssessmentBehaviours

class Assessment03 extends AssessmentBehaviours:
  //val PATH = "files/assessment/ms03"   // Assessment file path
  val PATH = "files/assessment03files"   // Assessment file path
  performTests(assessment.AssessmentMS03.create, "Milestone 3")