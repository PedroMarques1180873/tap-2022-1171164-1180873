package propertyBasedTests

import domain.*
import domain.SimpleTypes.*
import org.scalacheck.{Gen, Prop, Properties}

import java.time.{LocalDate, LocalTime}
import scala.collection.immutable
import org.scalacheck.Gen.const
import org.scalacheck.Prop.forAll

import java.time.format.DateTimeFormatter

object GeneratorSimpleTypes extends Properties("Gen Simple Types"):

  val genMinPreferenceValue = -2
  val genMaxPreferenceValue = 2
  val genMinDay = 1;
  val genMaxDay = 7;
  val genMinNurseSeniority = 1;
  val genMaxNurseSeniority = 5;
  val genMinLengthName = 3;
  val genMaxLengthName = 10;
  val genMinNurseRequirementNumber = 1;
  val genMaxNurseRequirementNumber = 5;
  val genMinConstraintNumber = 1;
  val genMaxConstraintNumber = 5;

  val validPeriods = Set("morning", "afternoon", "night");

  // Gen Preference Value simple type
  def genPreferenceValue: Gen[PreferenceValue] =
    for
      value             <- Gen.chooseNum(genMinPreferenceValue, genMaxPreferenceValue)
      preferenceValue   <- PreferenceValue.from(value.toString).fold(_ => Gen.fail, value => Gen.const(value))
    yield preferenceValue

  // Gen Day simple type
  def genDay: Gen[Day] =
    for
      value             <- Gen.chooseNum(genMinDay, genMaxDay)
      day               <- Day.from(value.toString).fold(_ => Gen.fail, value => Gen.const(value))
    yield day

  // Gen Nurse Seniority simple type
  def genNurseSeniority: Gen[NurseSeniority] =
    for
      value             <- Gen.chooseNum(genMinNurseSeniority, genMaxNurseSeniority)
      nurseSeniority    <- NurseSeniority.from(value.toString).fold(_ => Gen.fail, value => Gen.const(value))
    yield nurseSeniority

  // Gen Nurse Name Simple type
  def genNurseName: Gen[NurseName]  =
    for
      numChars              <- Gen.chooseNum(genMinLengthName, genMaxLengthName)
      value                 <- Gen.listOfN(numChars, Gen.alphaChar).map(_.mkString)
      nurseName             <- NurseName.from(value).fold(_ => Gen.fail, value => Gen.const(value))
    yield nurseName

  // Gen Nurse Requirement Number
  def genNurseRequirementNumber: Gen[NurseRequirementNumber] =
    for
      value                   <- Gen.chooseNum(genMinNurseRequirementNumber, genMaxNurseRequirementNumber)
      nurseRequirementNumber  <- NurseRequirementNumber.from(value.toString).fold(_ => Gen.fail, value => Gen.const(value))
    yield nurseRequirementNumber

  // Gen Role Simple type
  def genRole: Gen[Role]  =
    for
      numChars              <- Gen.chooseNum(genMinLengthName, genMaxLengthName)
      value                 <- Gen.listOfN(numChars, Gen.alphaChar).map(_.mkString)
      role             <- Role.from(value).fold(_ => Gen.fail, value => Gen.const(value))
    yield role

  // Gen Valid Period Simple type
  def genValidPeriod: Gen[ValidPeriod]  =
    for
      numChars                <- Gen.chooseNum(genMinLengthName, genMaxLengthName)
      value                   <- Gen.listOfN(numChars, Gen.alphaChar).map(_.mkString)
      validPeriod             <- ValidPeriod.from(value.toString()).fold(_ => Gen.fail, value => Gen.const(value))
    yield validPeriod

  // Gen Valid Period Simple type
  def genValidTime: Gen[ValidTime]  =
    for
      nanoOfDay     <- Gen.chooseNum(LocalTime.MIN.toNanoOfDay, LocalTime.MAX.toNanoOfDay)
      value         <- LocalTime.ofNanoOfDay(nanoOfDay);
      validTime     <- ValidTime.from(value.format(DateTimeFormatter.ofPattern("HH:mm:ss"))).fold(_ => Gen.fail, value => Gen.const(value))
    yield validTime

  // Gen Constraint Number Simple type
  def genConstraintNumber: Gen[ConstraintNumber] =
    for
      value                   <- Gen.chooseNum(genMinConstraintNumber, genMaxConstraintNumber)
      constraintNumber        <- ConstraintNumber.from(value.toString).fold(_ => Gen.fail, value => Gen.const(value))
    yield constraintNumber



  property("Property Test Simple Type") = forAll(genValidPeriod)(constraint =>{
    println(constraint)
    true
  })

/* List of generators availables for simpletypes
genConstraintNumber
genValidTime
genValidPeriod
genRole
genNurseRequirementNumber
genNurseName
genNurseSeniority
genDay
genPreferenceValue*/