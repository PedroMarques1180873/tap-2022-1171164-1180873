package propertyBasedTests

import domain.*
import domain.SimpleTypes.{ConstraintNumber, *}
import org.scalacheck.Prop.forAll
import org.scalacheck.Properties
import org.scalacheck.{Gen, Prop}
import propertyBasedTests.GeneratorSimpleTypes.*
import org.scalacheck.Properties
import domain.schedule.ScheduleMS01

object GeneratorDomainObjects extends Properties("Gen Domain Objects"):

  val genMinListRoles = 1;
  val genMaxListRoles = 10;
  val genMinNurseRequirements = 1;
  val genMaxNurseRequirements = 5;
  val genMinNumberPeriodPreferences = 1;
  val genMaxNumberPeriodPreferences = 3;
  val genMinNumberDayPreferences = 1;
  val genMaxNumberDayPreferences = 3;
  val genMinSchedulePeriod = 1;
  val genMaxSchedulePeriod = 10;
  val genMinPreferences = 1;
  val genMaxPreferences = 3;

  // Gen Constraint Object
  def genConstraint(maxshifts : Gen[ConstraintNumber]) : Gen[Constraint] =
    for
      minRestDaysPerWeek  <- genConstraintNumber;
      maxShiftsPerDay     <- maxshifts
    yield Constraint(minRestDaysPerWeek, maxShiftsPerDay)

  // Gen Nurse Requirement Object
  def genNurseRequirement : Gen[NurseRequirement] =
    for
      number    <- genNurseRequirementNumber;
      role      <- genRole
    yield NurseRequirement(number, role)

  // Gen Nurse Object
  def genNurse(listAllRoles: Seq[Role], listRoles: Seq[Role]) : Gen[Nurse] =
    for
      name            <- genNurseName;
      seniority       <- genNurseSeniority;
      numberRoles     <- Gen.chooseNum(0, listAllRoles.size/2) //Avoid getting the full list
      extraRoles <- Gen.pick(numberRoles,listAllRoles) //Obtain extra roles to add to the list
    yield Nurse(name, seniority, List.concat(listRoles,extraRoles).distinct)

  // Gen Schedule Period Object
  def genSchedulePeriod : Gen[SchedulePeriod] =
    for
      id                        <- genValidPeriod
      start                     <- genValidTime
      end                       <- genValidTime
      numberNurseRequirements   <- Gen.chooseNum(genMinNurseRequirements, genMaxNurseRequirements)
      listNurseRequirement      <- Gen.listOfN(numberNurseRequirements, genNurseRequirement)
    yield SchedulePeriod(id, start, end, listNurseRequirement)

  // Gen PeriodPref Object
  def genPeriodPref(listSP: List[SchedulePeriod], listNurse: List[Nurse]) : Gen[PeriodPref] =
    for
      //Obtain period from existing ones
      period            <- Gen.oneOf(listSP);
      //Obtain nurse from existing ones
      nurse             <- Gen.oneOf(listNurse)
      value             <- genPreferenceValue
    yield PeriodPref(period, nurse, value)

  // Gen DayPref Object
  def genDayPref(listNurse: List[Nurse]) : Gen[DayPref] =
    for
      day               <- genDay;
      //Obtain nurse from existing ones
      nurse             <- Gen.oneOf(listNurse)
      value             <- genPreferenceValue
    yield DayPref(nurse, day, value)

  //Gen Preferences Object
  def genPreferences(listSP: List[SchedulePeriod], listNurse: List[Nurse]) : Gen[Preferences] =
    for
      numberPeriodPreferences   <- Gen.chooseNum(genMinNumberPeriodPreferences, genMaxNumberPeriodPreferences);
      listPeriodPreferences     <- Gen.listOfN(numberPeriodPreferences, genPeriodPref(listSP, listNurse));
      numberDayPreferences      <- Gen.chooseNum(genMinNumberDayPreferences, genMaxNumberDayPreferences);
      listDayPreferences        <- Gen.listOfN(numberPeriodPreferences, genDayPref(listNurse))
    yield Preferences(listPeriodPreferences, listDayPreferences)

  //ListNurseRequirements flatten to List(Role,Role,Role,...) with duplicates
  //Método auxiliar utilizado em concatNRFromPeriods
  def flattenNR(listNR: Seq[NurseRequirement], index:Int, finalSeq: Seq[Role]) : Seq[Role] =
    if(index == listNR.size) finalSeq
    else
      val seqtoAdd : Seq[Role] = (1 to listNR(index).number.to).map(_ => listNR(index).role).toSeq
      val newFinal = (finalSeq :: seqtoAdd :: Nil).flatten
      flattenNR(listNR, index+1, newFinal)

  //Permite gerar Lista(Lista(Roles)) por Periodo por exemplo:   List(List(A,A,B,C,C),List(A,B,B,D))
  def concatNRFromPeriods(listNR: Seq[SchedulePeriod], index:Int, finalSeq: List[List[Role]]) : List[List[Role]] =
    if(index == listNR.size) finalSeq
    else
      val flattenNRresult : Seq[Role] = flattenNR(listNR(index).listNurseRequirement,0,Seq())
      val newFinal : List[List[Role]] = (finalSeq :+ flattenNRresult.toList)
      concatNRFromPeriods(listNR, index+1, newFinal)

  //Metodo obtido do stack overflow para remover elemento de uma lista de roles
  //Metodo auxiliar utilizado no recursiveMethodToObtainRolesForNurses
  def rm(xs: List[Role], value: Role): List[Role] = xs match
    case `value` :: tail =>  tail
    case x :: tail => x :: rm(tail, value)
    case _ => Nil


  //recursiveMethodToObtainRolesForNurses
  /*
      Ex: List(
        List(Role("A"),Role("A"),Role("B")),
        List(Role("C"),Role("B")),
        List(Role("B"),Role("D"))
        )
        Constraint = 2

  Result :
      List(
        List(Role(A), Role(C)),
        List(Role(A), Role(B)),
        List(Role(B), Role(B)),
        List(Role(D))
  )
  */
  def recursiveMethodToObtainRolesForNurses(listPeriodsRequirements: List[List[Role]], result: List[List[Role]], maxshifts: Int) : List[List[Role]] =
    if(listPeriodsRequirements.find(x => !x.isEmpty).isEmpty) result
    else
      //obtem primeira role das primeiras listas consoante constraint
      val listsRetiradas = (1 to (if(maxshifts > listPeriodsRequirements.size) listPeriodsRequirements.size else maxshifts)).map( i => listPeriodsRequirements(i-1)).toList

      val listRolesObtidas = listsRetiradas.map( lrole => lrole(0))

      //Das ListasRetiradas, obtem listas sem os primeiros elementos
      val listRetiradasFix = (1 to listsRetiradas.size).map(i => rm(listsRetiradas(i-1),listRolesObtidas(i-1))).toList

      //Obtem tamanho da listaRolesObtidas
      val tamanho = (if(maxshifts > listPeriodsRequirements.size) listPeriodsRequirements.size else maxshifts)

      //Obtem as listas de listaPeriodsRequirements que não foram tocadas
      val listRestantes = listPeriodsRequirements.toList.slice(tamanho,listPeriodsRequirements.size)

      //Agrega primeiro as listasRetiradas sem a head com as Restantes que não foram tocadas.
      //Remove qualquer uma que tenha ficado vazia
      val listaResultadoPeriodRequirements = List.concat(listRetiradasFix,listRestantes).filter(x => !x.isEmpty)

      recursiveMethodToObtainRolesForNurses(listaResultadoPeriodRequirements, result :+ listRolesObtidas,maxshifts)

  //Gen Schedule Info
  def genScheduleInfo : Gen[ScheduleInfo] =
    for
      //Gerar List Schedule Period
      numberSchedulePeriod   <- Gen.chooseNum(genMinSchedulePeriod, genMaxSchedulePeriod)
      listSchedulePeriod     <- Gen.listOfN(numberSchedulePeriod, genSchedulePeriod)

      //Gen max shift per day constraint
      maxshifts <- genConstraintNumber

      //ListSchedulePeriod flatten to List(List(Role,Role),List(Role,...) with duplicates
      // List(Period.listNRExtensa.flatten)
      listRolesByPeriod = concatNRFromPeriods(listSchedulePeriod,0,List())

      //Obtem lista de roles que vão ser atribuidas aos enfermeiros de acordo com a constraint
      listRoles = recursiveMethodToObtainRolesForNurses(listRolesByPeriod,List(),maxshifts.to)

      //Determina quais as roles existentes para adicionar roles extras aos enfermeiros
      listTodasRoles = listRolesByPeriod.flatten.distinct

      //Consoante o numero de lista de Roles obtidas será gerado enfermeiros
      listNursesGenerated = (1 to listRoles.size).map( i => genNurse(listTodasRoles,listRoles(i-1))).toList

      listNurses <- Gen.sequence[List[Nurse],Nurse](listNursesGenerated)

      ListPreferences <- Gen.listOfN(1,genPreferences(listSchedulePeriod, listNurses)) //Sempre Lista com um elemento
      ListConstraints <- Gen.listOfN(1,genConstraint(maxshifts)) //Sempre Lista com um elemento
    yield ScheduleInfo(listSchedulePeriod,listNurses,ListPreferences.head,ListConstraints.head)

  //Properties
  property("Validate ScheduleInfo") = forAll(genScheduleInfo)(scheduleInfo =>{
    //println(scheduleInfo)
    ScheduleMS01.geraHorario(scheduleInfo).fold(l => false, r => true)
  })

  property("Os Shifts apresentam os mesmos identificadores de periodos gerados antes") = forAll(genScheduleInfo)(scheduleInfo => {
    ScheduleMS01.geraHorario(scheduleInfo).fold(l => false,
      lshifts => !lshifts.find(
        s =>s.find(
          s => scheduleInfo.listSchedulePeriod.find(
            x => x.id == s.id
          ).isDefined
        ).contains(false)
      ).isDefined
    )
  })

  property("Os Shifts apresentam os mesmos enfermeiros gerados antes") = forAll(genScheduleInfo)(scheduleInfo => {
    ScheduleMS01.geraHorario(scheduleInfo).fold(l => false,
      lshifts => !lshifts.find(
        s =>s.find(
          s => scheduleInfo.ListNurses.find(
            x => s.seqNurseRole.map(
              y=> y._1
            ).contains(x.name)
          ).isDefined
        ).contains(false)
      ).isDefined
    )
  })

  property("Os Shifts apresentam os mesmos roles gerados antes") = forAll(genScheduleInfo)(scheduleInfo => {
    ScheduleMS01.geraHorario(scheduleInfo).fold(l => false,
      lshifts => !lshifts.find(
        s =>s.find(
          s => scheduleInfo.ListNurses.map(
            r => r.listRoles
          ).find(
            x => s.seqNurseRole.map(
              y=> y._2
            ).contains(x.to)
          ).isDefined
        ).contains(false)
      ).isDefined
    )
  })

  property("The shift schedule must schedule all the requirements for each day") = forAll(genScheduleInfo)(scheduleInfo => {
    ScheduleMS01.geraHorario(scheduleInfo).fold(l => false,
      lshifts => lshifts.forall(
        ls => ls.forall(
          s =>
              scheduleInfo.listSchedulePeriod.find(p => p.id == s.id).get.listNurseRequirement.forall(
              nr => s.seqNurseRole.groupBy(x => x._2).map(t => (t._1, t._2.length)).get(nr.role) match{
                case Some(right) =>  right == nr.number.to
                case None => false
              }
            )
        )
      )
    )
  })

  property("Nurse cannot be used in the same shift with different roles") = forAll(genScheduleInfo)(scheduleInfo =>{
    ScheduleMS01.geraHorario(scheduleInfo).fold(l => false,

      seqSeqShift => seqSeqShift.forall(
        seqShift => seqShift.forall(

          shift => shift.seqNurseRole.map(
            tuplet => tuplet._1
          ).toList.distinct.size == shift.seqNurseRole.size


        )
      )
    )
  })

  property("Constraint maxShiftPerDay not violated") = forAll(genScheduleInfo)(scheduleInfo =>{
    ScheduleMS01.geraHorario(scheduleInfo).fold(l => false,

      seqSeqShift => seqSeqShift.forall(
        //Group de seqShifts by nurse resulting in a map with the number of shifts each nurse did  Map(Nurse -> X Shifts)
        seqShift => seqShift.map(s => s.seqNurseRole).flatten[(Nurse,Role)].groupBy(t => t._1).map(t => (t._1, t._2.length)).forall(
          t => t._2 <= scheduleInfo.constraint.maxShiftsPerDay.to
        )
      )
    )
  })

  property("O dominio gerado possui Preferences de Days para Nurses Válidos") = forAll(genScheduleInfo)(scheduleInfo => {
    !List(scheduleInfo.preferences).map(
      lp => (lp.listDayPreferences, lp.listPeriodPreferences)
    ).find(
        p =>p._1.find(
          p => scheduleInfo.ListNurses.find(
            n => n.name == p.nurse.name
          ).isDefined
        ).contains(false)
      ).isDefined
  })

  property("O dominio gerado possui Preferences de Periods para Nurses Válidos") = forAll(genScheduleInfo)(scheduleInfo => {
    !List(scheduleInfo.preferences).map(
      lp => (lp.listDayPreferences, lp.listPeriodPreferences)
    ).find(
      p =>p._2.find(
        p => scheduleInfo.ListNurses.find(
          n => n.name == p.nurse.name
        ).isDefined
      ).contains(false)
    ).isDefined
  })

  property("O dominio gerado possui Preferences de Periods para Schedule Periods Válidos") = forAll(genScheduleInfo)(scheduleInfo => {
    !List(scheduleInfo.preferences).map(
      lp => (lp.listDayPreferences, lp.listPeriodPreferences)
    ).find(
      p =>p._2.find(
        p => scheduleInfo.listSchedulePeriod.find(
          sp => sp.id == p.period.id
        ).isDefined
      ).contains(false)
    ).isDefined
  })

  property("Os Nurse Requirements de cada Schedule Period têm de satisfazer os Roles dos Enfermeiros") = forAll(genScheduleInfo)(scheduleInfo => {
    scheduleInfo.listSchedulePeriod.map(
      sp => sp.listNurseRequirement
    ).flatten.forall(
      nr => scheduleInfo.ListNurses.filter(n => n.listRoles.contains(nr.role)).size >= nr.number.to
    )
  })

  property("Constraint Min Rest Days Per Week not violated - should fail") = forAll(genScheduleInfo)(scheduleInfo => {
    scheduleInfo.ListNurses.forall( nurse =>
      ScheduleMS01.geraHorario(scheduleInfo).fold(left => false, seqSeqShifts =>

        //Para todos os dias
        seqSeqShifts.filter( seqShifts =>

          seqShifts.filter( shift =>

            //Verifica se o enfermeiro fez turno
            shift.seqNurseRole.find(t => t._1 == nurse).isDefined

          ).size > 0 // Verifica se encontrou nurse naquele dia(Seq[Shifts])

        ).size <= 7-scheduleInfo.constraint.minRestDaysPerWeek.to //Verifica o numero de dias de trabalho do enfermeiro
        // é inferior ou igual aos dias da semana menos os dias de descanso
      )
    )
  })


