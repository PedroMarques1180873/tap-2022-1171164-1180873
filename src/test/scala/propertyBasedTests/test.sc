

case class Nurse(name:String,lr:List[String])
case class NR(role:String,number:Int)
case class Period(listNR: List[NR])
case class ScheduleDay(shifts: List[List[(Nurse,String)]], ln: List[Nurse], peso:Int)
case class ConstraintNumber(peso:Int)

val constraint = 1
val CDay = ConstraintNumber(2)

val ln:List[Nurse] = List(
  Nurse("N1",List("A","B","C")),
  Nurse("N2",List("A")),
  Nurse("N3",List("B")),
  Nurse("N4",List("A","B","C")),
  Nurse("N5",List("C")),
  Nurse("N6",List("B")),
  //Nurse("N7",List("A","B","C")),
  //Nurse("N8",List("A")),
  //Nurse("N9",List("B")),

)

//1º periodo
val lr: List[NR] = List(
  NR("A",2),
  NR("B",1)
)

//2º periodo
val lr2: List[NR] = List(
  NR("C",1),
  NR("B",2)
)

val lperiodos: List[Period] = List(
  Period(lr),
  Period(lr2)
)


/**
 * Obtem combinação de enfermeiros para um respetivo Nurse Requirement
 */
def nurseCombinationForRequirement(ln: List[Nurse])(r: NR): List[List[(Nurse,String)]] =
  val lnf = ln.filter(_.lr.contains(r.role))
  lnf.map(n => (n,r.role)).combinations(r.number).toList


nurseCombinationForRequirement(ln)(lr.head)

lr.map(nurseCombinationForRequirement(ln))



def combineSets(ll: List[List[(Nurse,String)]], lncr: List[List[(Nurse,String)]]): List[List[(Nurse,String)]] =
  for
    l <- ll
    ncr <- lncr
    if(l.forall( (n,_) =>  !ncr.exists( (na,_) => n == na)))
  yield l ++ ncr



def nurseCombinationForRequirementsFold(ln: List[Nurse], lr:List[NR], ll: List[List[(Nurse,String)]]): List[List[(Nurse,String)]] =
  lr.foldLeft[List[List[(Nurse,String)]]](List(List()))((ll,r) =>
    combineSets(ll, nurseCombinationForRequirement(ln)(lr.head))
)

def nurseCombinationForRequirements(ln: List[Nurse], lr: List[NR], ll: List[List[(Nurse,String)]]) : List[List[(Nurse,String)]] =
  if(lr.isEmpty) ll else
    val lncr = nurseCombinationForRequirement(ln)(lr.head)
    val nll = combineSets(ll,lncr)
    nurseCombinationForRequirements(ln,lr.tail,nll)


//Combinações existentes para um periodo que nao permite enfermeiro efetuar mais de uma role
nurseCombinationForRequirements(ln,lr,List(List()))


/***   Combinações possiveis de periodos para um dia ***/

def combinePeriodosSets(llshifts: List[List[List[(Nurse,String)]]], combPeriodo: List[List[(Nurse,String)]]): List[List[List[(Nurse,String)]]] =
  for
    seqPeriodo <- llshifts
    shift <- combPeriodo

    tryCombinePeriods = seqPeriodo :+ shift
    if({
      val list : Map[String, Int] = tryCombinePeriods.map( shift => shift.map( (n,_) => n._1)).flatten.groupBy(name => name).map(n => (n._1, n._2.length))
      list.forall(_._2 <= constraint)
    })
  yield seqPeriodo :+ shift




def nurseCombinationForPeriods(ln: List[Nurse], lperiodos: List[Period], llshifts : List[List[List[(Nurse,String)]]]) : List[List[List[(Nurse,String)]]] =
  if(lperiodos.isEmpty) llshifts else
    val lshifts = nurseCombinationForRequirements(ln,lperiodos.head.listNR,List(List()))
    val llshiftsCombined = combinePeriodosSets(llshifts,lshifts)
    nurseCombinationForPeriods(ln,lperiodos.tail,llshiftsCombined)


val result = nurseCombinationForPeriods(ln,lperiodos,List(List()))


//print(result)

val listDias = result.map( ll => {
  val lns = ll.map( shift => shift.map(n => n._1)).flatten.distinct
  ScheduleDay(ll,lns,0)
})

//print(listDias)

print(listDias.length)




type Result[A] = Either[String,A]

//Map with Constraints to 7-ConstraintNumber = Work Days for each nurse
def getMapWithListNurseAndWorkDays( listNursesToValidate: List[Nurse], constraint: ConstraintNumber) : Map[Nurse, Int] =
  listNursesToValidate.map( n => (n, 7-constraint.peso)).toMap

def validateMapWithListNurse(listNursesToValidate : List[Nurse], mapXPTO : Map[Nurse, Int]) : (List[Nurse] , Map[Nurse, Int]) =

  //Update Mapa
  val newMap : Map[Nurse, Int] = mapXPTO.map( (n,WorkDaysLeft) => if( listNursesToValidate.contains(n) ) (n,WorkDaysLeft-1) else (n,WorkDaysLeft) ).toMap

  //Obtem os enfermeiros q passaram dos limites
  val listNurses = newMap.foldRight[List[Nurse]](List()) ( (m,listNurses) => if( m._2 == 0 ) listNurses :+ m._1 else listNurses )

  (listNurses,newMap)


//Retira da lista de dias dados, todos os que contenham algum dos enfermeiros
def retirarDaListDias(listDays: List[List[Nurse]] , listNurses: List[Nurse]) : List[List[Nurse]] =
  listDays.filter( day => day.forall( n => !listNurses.contains(n)))


def obterRestantesLista(day : List[Nurse], list: List[List[Nurse]]) : List[List[Nurse]] =
  if(day == list.head) list.tail else
    obterRestantesLista(day, list.tail)



//Metodo recursivo para obter lista de semanas possiveis
def obtainValidWeek(listOrderedDays: List[List[Nurse]], mapWithWorkDays: Map[Nurse, Int], validweek: List[List[Nurse]]) : Result[List[List[List[Nurse]]]] =

  if(listOrderedDays.isEmpty) return Left("NotEnoughNurses")

  if(validweek.length == 7) Right(List(validweek)) else

    val semanas = listOrderedDays.foldLeft[List[List[List[Nurse]]]](List())((semanas,day) => {
      //val listDiasRestante : List[Dia] = obterRestantesLista(day,listOrderedDays)

      val mapvalidado = validateMapWithListNurse(day,mapWithWorkDays)

      //Retirar da lista de Dias os enfermeiros q já nao podem trabalhar
      if(mapvalidado._1.isEmpty) then
        //Valido ln
        val listSemAnteriores = day +: obterRestantesLista(day,listOrderedDays)

        val semana = obtainValidWeek(listSemAnteriores,mapvalidado._2,validweek :+ day)
        semana.fold(left => semanas, semana => semanas :+ semana.head)
      else
        //Invalido
        val listDiasRestante : List[List[Nurse]] = obterRestantesLista(day,listOrderedDays)
        val restantesDaListFiltered = retirarDaListDias(listDiasRestante, mapvalidado._1) //Obter dias que nao têm nurses que ja estao ocupados
        val semana = obtainValidWeek(restantesDaListFiltered,mapWithWorkDays,validweek)
        semana.fold(left => semanas, semana => semanas :+ semana.head)


    })

    if(!semanas.isEmpty) Right(semanas) else Left("NotEnoughNurses")





def obtainValidWeeks(listOrderedDays: List[ScheduleDay], constraint: ConstraintNumber, listNurses: List[Nurse]) : Result[List[List[List[Nurse]]]] =



  val  mapWithWorkDays: Map[Nurse, Int] = getMapWithListNurseAndWorkDays(listNurses ,constraint)


  val listDayNurse = listOrderedDays.map(d => d.ln.sortBy(n => n.name)).distinct


  val listWeeks = obtainValidWeek(listDayNurse, mapWithWorkDays,List())
  listWeeks.fold(left => Left(left), week => Right(week))




val test24 = listDias.length
val listnurses = listDias.map(d => d.ln.sortBy(n => n.name)).distinct.length

//val listWeeks = obtainValidWeeks(listDias, CDay,ln)



def obtemDiasByNurses(listOrderedDays: List[ScheduleDay], listWeeks: List[List[List[Nurse]]]) : List[List[ScheduleDay]] =

  val listNursesFlatten = listWeeks.flatten.flatten.distinct

  val mapa = listOrderedDays.groupBy(s => s.ln.sortBy(n => n.name)).map(t => (t._1,t._2.maxBy(_.peso)))

  val result = listWeeks.map( d => {
    d.map( ln => {
      mapa.get(ln) match {
        case Some(x) => x
        case None    => ScheduleDay(List(),List(),0)
      }
    })
  })
  return result
//val nurses : List[(Int, List[Nurse])] = listDias.zipWithIndex.map( (e, i) => (i,e.ln.sortBy(n => n.name)) )

//val listNurses = nurses.distinctBy((i,ln) => ln)

//val listNursesFlatten = listWeeks.flatten.flatten.distinct

val mapa = listDias.groupBy(s => s.ln.sortBy(n => n.name)).map(t => (t._1,t._2.maxBy(_.peso)))
println(mapa)

def remove(num: Int, list: List[Nurse]) = list diff List(num)


val resul = remove(1,ln)

List(0,4).foldRight[List[Int]](List(0,1,2,3,4,5))( (i,map) => remove(i,map))

