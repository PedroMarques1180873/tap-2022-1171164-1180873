package domain

import domain.SimpleTypes.*
import xml.XML.fromAttribute

import scala.xml.Node
import scala.annotation.targetName

final case class Constraint(minRestDaysPerWeek: ConstraintNumber, maxShiftsPerDay: ConstraintNumber)

object Constraint:

  //Atributos da Constraint
  val minRestDaysAttribute: String = "minRestDaysPerWeek"
  val maxShiftsAttribute: String = "maxShiftsPerDay"

  def from(xml: Node): Result[Constraint] =
    for {
      minRestDays <- fromAttribute(xml, minRestDaysAttribute).fold[Result[ConstraintNumber]](left => Left(left), StringValue => ConstraintNumber.from(StringValue))
      maxShifts <- fromAttribute(xml, maxShiftsAttribute).fold[Result[ConstraintNumber]](left => Left(left), StringValue => ConstraintNumber.from(StringValue))
    } yield Constraint(minRestDays, maxShifts)





