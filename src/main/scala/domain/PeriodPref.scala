package domain

import scala.xml.Node
import xml.XML.traverse
import xml.XML.fromAttribute
import domain.DomainError.*
import domain.SimpleTypes.*

final case class PeriodPref(period: SchedulePeriod, nurse: Nurse, value: PreferenceValue)

object PeriodPref:

  val valueAttribute: String = "value"
  val nurseAttribute: String = "nurse"
  val periodAttribute: String = "period"

  def obtainNurse(listNurses: Seq[Nurse], nurseRead : String) : Result[Nurse]=
    for {
      result <- listNurses.find(x => x.to == nurseRead).fold(Left(UnknownNurse(nurseRead)))( x => Right(x))
    } yield result

  def obtainSchedulePeriod(listPeriods: Seq[SchedulePeriod], periodRead : String) : Result[SchedulePeriod]=
    for {
      result <- listPeriods.find(x => x.to == periodRead).fold(Left(UnknownPeriod(periodRead)))( x => Right(x))
    } yield result

  def from(xml : Node, listPeriods : Seq[SchedulePeriod], listNurses: Seq[Nurse]) : Result[PeriodPref] =
    for {
      periodRead <- fromAttribute(xml, periodAttribute)
      period <- obtainSchedulePeriod(listPeriods, periodRead)
      nurseRead <- fromAttribute(xml, nurseAttribute)
      nurse <- obtainNurse(listNurses, nurseRead)
      value <- fromAttribute(xml, valueAttribute).fold[Result[PreferenceValue]](left => Left(left), StringValue => PreferenceValue.from(StringValue))
    } yield PeriodPref(period, nurse, value)