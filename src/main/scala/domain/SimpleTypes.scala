package domain

import scala.annotation.targetName
import domain.DomainError.*

object SimpleTypes:

  opaque type PreferenceValue = Int
  object PreferenceValue:
    def from(s: String): Result[PreferenceValue] =
      try
        val prefVal = s.toInt
        if ( -2 <= prefVal && prefVal <= 2) Right(prefVal) else Left(PreferenceValueOutOfRange(s))
      catch
        case ex : NumberFormatException => Left(InvalidPreferenceValueFormat(s))
  extension(value: PreferenceValue)
    @targetName("toValue")
    def to: Int = value


  opaque type Day = Int
  object Day:
    def from(s: String): Result[Day] =
      try
        val Day = s.toInt
        if ( 1 <= Day && Day <= 7) Right(Day) else Left(InvalidDayOutOfRange(s))
      catch
        case ex : NumberFormatException => Left(InvalidDayFormat(s))
  extension(value: Day)
    @targetName("toNumberDay")
    def to: Int = value

  opaque type NurseSeniority = Int
  object NurseSeniority:
    def from(s: String): Result[NurseSeniority] =
      try
        val senVal = s.toInt
        if ( 1 <= senVal && senVal <= 5) Right(senVal) else Left(SeniorityValueOutOfRange(s))
      catch
        case ex : NumberFormatException => Left(InvalidSeniorityValueFormat(s))
  extension(value: NurseSeniority)
    @targetName("toSeniorityValue")
    def to: Int = value

  opaque type NurseName = String
  object NurseName:
    def from(s: String): Result[NurseName] =
      if !s.trim.isEmpty then Right(s) else Left(EmptyName)
  extension(s: NurseName)
    @targetName("toNurseName")
    def to: String = s

  opaque type NurseRequirementNumber = Int
  object NurseRequirementNumber:
    def from(s: String): Result[NurseRequirementNumber] =
      try
        val Number = s.toInt
        if ( Number > 0) Right(Number) else Left(InvalidNurseRequirementNumberOutOfRange(s))
      catch
        case ex : NumberFormatException => Left(InvalidNurseRequirementNumberFormat(s))
  extension(value: NurseRequirementNumber)
    @targetName("toNurseRequirementNumber")
    def to: Int = value

  opaque type Role = String
  object Role:
    def from(s: String): Result[Role] =
      if !s.trim.isEmpty then Right(s) else Left(InvalidUnkownRole(s))
  extension(s: Role)
    @targetName("toRoleName")
    def to: String = s

  opaque type ValidPeriod = String
  object ValidPeriod:
    def from(s: String): Result[ValidPeriod] =
      //if (s.equals("morning") || s.equals("afternoon") || s.equals("night")) then Right(s) else Left(InvalidPeriod(s))
      if !s.trim.isEmpty then Right(s) else Left(InvalidPeriod(s))
  extension(s: ValidPeriod)
    @targetName("toPeriod")
    def to: String = s

  opaque type ValidTime = String
  object ValidTime:
    def from(s: String): Result[ValidTime] =
      if (s.matches("^(?:(?:([01]?\\d|2[0-3]):)?([0-5]?\\d):)?([0-5]?\\d)$")) then Right(s) else Left(InvalidTimeFormat(s))
  extension(s: ValidTime)
    @targetName("toTime")
    def to: String = s

  opaque type ConstraintNumber = Int
  object ConstraintNumber:
    def from(s: String): Result[ConstraintNumber] =
      try
        val Number = s.toInt
        if ( 0 <= Number) then Right(Number) else Left(InvalidConstraintNumberOutOfRange(s))
      catch
        case ex : NumberFormatException => Left(InvalidConstraintNumberFormat(s))
  extension(value: ConstraintNumber)
    @targetName("toConstraintNumber")
    def to: Int = value