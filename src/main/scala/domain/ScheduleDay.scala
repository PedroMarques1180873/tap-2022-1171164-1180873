package domain

import domain.SimpleTypes.{Role, ValidPeriod}
import domain.*

final case class ScheduleDay (listShifts: List[Shift], listNurses: List[Nurse], weight: Int)

object ScheduleDay:

  val nurseRequirementNode: String = "nurseRequirement"
  val idAttribute: String = "id"
  val startAttribute: String = "start"
  val endAttribute: String = "end"

  def from(listlistTuplet: List[(ValidPeriod,List[(Nurse,Role)])], preferences: Preferences): Result[ScheduleDay] =

    val listNurses = listlistTuplet.map( shift => shift._2.map(n => n._1)).flatten.distinct

    val resultlistShifts = listlistTuplet.map(shift => Shift.from(shift._1.to,shift._2,preferences.listPeriodPreferences)).toList

    val listShifts = convert(resultlistShifts)
    for
      listShifts <- convert(resultlistShifts)
      weight = listShifts.foldRight[Int](0)( (s,totalWeight) => s.weight + totalWeight)
    yield ScheduleDay(listShifts,listNurses, weight)


  //Metodo q permite converter List[Result] into Result[List]
  def convert[A](p: List[Result[A]]): Result[List[A]] =
    p collectFirst {
      case Left(a) => a
    } toLeft {
      p collect {
        case Right(b) => b
      }
    }
