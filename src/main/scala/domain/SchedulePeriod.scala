package domain

import domain.*
import domain.SimpleTypes.*

import scala.xml.Node
import xml.XML.traverse
import xml.XML.fromAttribute

import scala.annotation.targetName

final case class SchedulePeriod (id: ValidPeriod, start: ValidTime, end: ValidTime, listNurseRequirement: Seq[NurseRequirement])

object SchedulePeriod:

  val nurseRequirementNode: String = "nurseRequirement"
  val idAttribute: String = "id"
  val startAttribute: String = "start"
  val endAttribute: String = "end"

  def from(xml: Node): Result[SchedulePeriod] =
    for {
      id <- fromAttribute(xml, idAttribute).fold[Result[ValidPeriod]](left => Left(left), StringValue => ValidPeriod.from(StringValue))
      start <- fromAttribute(xml, startAttribute).fold[Result[ValidTime]](left => Left(left), StringValue => ValidTime.from(StringValue))
      end <- fromAttribute(xml, endAttribute).fold[Result[ValidTime]](left => Left(left), StringValue => ValidTime.from(StringValue))
      lnr <- traverse((xml \\ nurseRequirementNode), (n:Node) => NurseRequirement.from(n))
    } yield SchedulePeriod(id, start, end, lnr)
    
  extension(period: SchedulePeriod)
    @targetName("toStringPeriodId")
    def to: String = period.id.to
