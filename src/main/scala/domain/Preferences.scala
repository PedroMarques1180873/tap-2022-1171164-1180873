package domain

import scala.xml.Node
import xml.XML.traverse
import xml.XML.fromAttribute
import domain.DomainError.*
import domain.SimpleTypes.*

final case class Preferences(listPeriodPreferences: Seq[PeriodPref], listDayPreferences: Seq[DayPref])

object Preferences:
  
  val listPeriodsAttribute : String = "periodPreference"
  val listDaysAttribute  : String = "dayPreference"

  def from(xml: Node, listPeriods : Seq[SchedulePeriod], listNurses: Seq[Nurse]): Result[Preferences] =
    for {
      listPeriod <- traverse((xml \\ listPeriodsAttribute), (n:Node) => PeriodPref.from(n, listPeriods, listNurses))
      listDay <- traverse((xml \\ listDaysAttribute), (n:Node) => DayPref.from(n, listNurses))
    } yield Preferences(listPeriod, listDay)