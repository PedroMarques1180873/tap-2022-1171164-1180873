package domain.schedule

import scala.xml.Elem
import domain.{DomainError, Nurse, NurseRequirement, Preferences, Result, ScheduleInfo, SchedulePeriod, Shift}
import domain.DomainError.*
import domain.SimpleTypes.Role


object ScheduleMS01 extends Schedule:

  // TODO: Create the code to implement a functional domain model for schedule creation
  //       Use the xml.XML code to handle the xml elements
  //       Refer to https://github.com/scala/scala-xml/wiki/XML-Processing for xml creation
  def create(xml: Elem): Result[Elem] =
    ScheduleInfo.from(xml) match
      case Left(x) => Left(x)//Left(exportToXMLError(x))
      case Right(z) => geraHorario(z) match
        case Left(l) => Left(l)//Left(exportToXMLError(l))
        case Right(r) => Right(exportToXML(r))

  //Metodo q permite converter List[Result] into Result[List]
  def convert[A](p: Seq[Result[A]]): Result[Seq[A]] =
    p collectFirst {
      case Left(a) => a
    } toLeft {
      p collect {
        case Right(b) => b
      }
    }

  def geraHorario(scheduleInfo: ScheduleInfo) : Result[Seq[Seq[Shift]]] =
    val seqSeqShifts = (1 to 7).map(_ => geraHorarioDia(scheduleInfo)).toSeq

    convert(seqSeqShifts)


  def geraHorarioDia(scheduleInfo: ScheduleInfo) : Result[Seq[Shift]] =
    // Devolve algo do tipo: Seq[(String, Seq[NurseRequirement])]
    val listRequirementsByPeriod = scheduleInfo.listSchedulePeriod.map(x => (x.id, x.listNurseRequirement))

    //Devolve Seq[(String,Nurse)]
    val listaRolePessoas = scheduleInfo.ListNurses.map(t => t.listRoles map (a => (a,t))).flatten

    //Agrupa por role e devolve um mapa: Map[Role, Array[Nurse]]
    val mapFinal = listaRolePessoas.groupBy( (r,n) => r).map((K,Array) => (K,(Array.unzip)._2))

    //completa mapa anterior para os values conterem (Nurse,MaxShiftsPerDay), devolve: Map[Role, Array[(Nurse,MaxShiftsPerDay)]]
    val mapComNursesContraint = mapFinal.map((K,ArrayNurses) => (K, ArrayNurses.map(n => (n,scheduleInfo.constraint.maxShiftsPerDay.to))))

    //metodo recursivo que percorre todos os SchedulePeriods e devolve uma Seq[Seq[Shifts]]
    //Cada shift vai conter o periodo e os respetivos enfermeiros e a role a realiza-lo
    ForEachPeriodrecursive(scheduleInfo.preferences,scheduleInfo.listSchedulePeriod,0,mapComNursesContraint,Seq())


  def ForEachPeriodrecursive(preferences: Preferences, list: Seq[SchedulePeriod], index: Int, mapaUpdated: Map[Role, Seq[(Nurse,Int)]], SeqShifts: Seq[Shift]) : Result[Seq[Shift]] =

  //Condição de paragem quando percorrer todos os SchedulePeriods
    if(list.size == index) then Right(SeqShifts)
    else
      val period = list(index)
      val resultadoFinal = for {
        //Gera horario para um respetivo periodo através da funçao recursiva
        resultGerarHorarioPeriod <- gerarHorarioPeriodoDeDia(preferences, period.id.to,period.listNurseRequirement,0,mapaUpdated,Seq())
      } yield ForEachPeriodrecursive(preferences, list,index+1,resultGerarHorarioPeriod._2,SeqShifts :+ resultGerarHorarioPeriod._1)

      resultadoFinal.flatten.fold(er => Left(er), resul => Right(resul))

  def gerarHorarioPeriodoDeDia(preferences: Preferences, id: String, seqNR: Seq[NurseRequirement], index: Int, MapaUpdated: Map[Role, Seq[(Nurse,Int)]], seqShift: Seq[Shift])  : Result[(Shift,Map[Role, Seq[(Nurse,Int)]])] =
  // Condi�ao Paragem
  // Atira Seq[Shift(id,Seq[(Nurse,Role)])]
    if(seqNR.size == index)
    then
      //Agrupa shifts existentes por periodo
      val seqAppendedShiftsPeriod = seqShift.foldLeft(Seq[Seq[(Nurse,Role)]]()){
        case (seqTuplet, i) => seqTuplet :+ i.seqNurseRole
      }.flatten.sortWith(_._1.name.to < _._1.name.to)
      Shift.from(id,seqAppendedShiftsPeriod,preferences.listPeriodPreferences).fold(Left(_),x => Right(x,MapaUpdated))
    else
      //Obtem o NurseRequirement
      val nr = seqNR(index)

      val resultfinal = for{
        // Obtem enfermeiros que satisfaçam o nurse requirement, impedindo o mesmo de executar mais que uma role por periodo
        seqNurseResult <- obtainNurses(id,seqShift,nr, MapaUpdated)
        // Atualiza o valor das constraints para os enfermeiros obtidos
        mapUpdatedAgain <- updateMapWithNewNurseShift(seqNurseResult, MapaUpdated)
        //Criar Shifts (id, Seq[Nurses, Role])
        shift <- Shift.from(id, seqNurseResult.map(n => (n,nr.role)),preferences.listPeriodPreferences)
        //Atualiza Seq de Shifts existente no periodo
      } yield gerarHorarioPeriodoDeDia(preferences, id,seqNR,index+1,mapUpdatedAgain,seqShift:+shift)

      resultfinal.flatten.fold(er => Left(er), resul => Right(resul))

  def obtainNurses(id: String, seqShifts: Seq[Shift],nurseRequirement : NurseRequirement, mapXPTO : Map[Role, Seq[(Nurse, Int)]]) : Result[Seq[Nurse]] =
    //Obtain information about nurse requirement
    val role = nurseRequirement.role
    val numberForRole = nurseRequirement.number.to

    //Obter enfermeiros que facam role
    val seqNurseConstraint = mapXPTO(role)

    //Obter enfermeiros que ja estao em servico no periodo
    val seqNurseOcupados = seqShifts.filter(f => f.id.to == id).map(x => x.seqNurseRole.map(y => y._1.name.to)).flatten

    //Obtem enfermeiros disponiveis
    val seqNursesDisponiveis = seqNurseConstraint.filter(x => x._2 != 0 && !seqNurseOcupados.contains(x._1.name.to))
    val nursesWithInt = seqNursesDisponiveis.take(numberForRole)
    val nursesToReturn = (nursesWithInt.unzip)._1

    //Validação se obteve o numero necessario de enfermeiros
    if(nursesToReturn.length != numberForRole)
      Left(NotEnoughNurses)
    else Right(nursesToReturn)


  def updateMapWithNewNurseShift( seqNurses: Seq[Nurse], mapXPTO : Map[Role, Seq[(Nurse,Int)]]) : Result[Map[Role, Seq[(Nurse, Int)]]] =
  //Decresce valor da constraint do maximo numero de shifts por dia
    Right(mapXPTO.map((K,ArrayNursesConst) => (K, ArrayNursesConst.map((n,c) => if(seqNurses.contains(n)) then (n,c-1) else (n,c)))))

  def exportToXML(seqSeqShift: Seq[Seq[Shift]]): Elem =
    <shiftSchedule xmlns="http://www.dei.isep.ipp.pt/tap-2022" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.dei.isep.ipp.pt/tap-2022 ../../schedule.xsd ">
      {(1 to seqSeqShift.size).map(i => exportXML2(seqSeqShift(i-1),i))}
    </shiftSchedule>

  def exportXML2(SeqShift: Seq[Shift], i: Int): Elem =
    <day id={i.toString}>
      {SeqShift.map(Elem => Shift.exportToXML(Elem))}
    </day>

  def exportToXMLError(error: DomainError): Elem =
      <ScheduleError xmlns="http://www.dei.isep.ipp.pt/tap-2022" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.dei.isep.ipp.pt/tap-2022 ../../scheduleError.xsd "
                     message={error.toString}/>