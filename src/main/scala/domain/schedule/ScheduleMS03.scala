package domain.schedule

import scala.xml.Elem
import domain.*
import domain.SimpleTypes.*
import domain.DomainError.*
import org.omg.CORBA.DynAnyPackage.InvalidValue


object ScheduleMS03 extends Schedule:

  // TODO: Create the code to implement a functional domain model for schedule creation
  //       Use the xml.XML code to handle the xml elements
  //       Refer to https://github.com/scala/scala-xml/wiki/XML-Processing for xml creation
  def create(xml: Elem): Result[Elem] =
    ScheduleInfo.from(xml) match
      case Left(x) => Left(x)//Left(exportToXMLError(x))
      case Right(z) => geraHorario(z) match
        case Left(l) => Left(l)//Left(exportToXMLError(l))
        case Right(r) => Right(exportToXML(r))

  //Metodo q permite converter List[Result] into Result[List]
  def convert[A](p: Seq[Result[A]]): Result[Seq[A]] =
    p collectFirst {
      case Left(a) => a
    } toLeft {
      p collect {
        case Right(b) => b
      }
    }

  def getBestWeekByScheduleDayWeight( multipleWeeks :  List[List[ScheduleDay]]) :  List[List[ScheduleDay]] =
    val WeeksWeights = multipleWeeks.map( week => {
      println( "Weights")
      val weightWeek = week.foldLeft[Int](0)((soma,scheduleDay) => soma + scheduleDay.weight)
      print( weightWeek+", ")
      (week,weightWeek)
    })
    val pesos = WeeksWeights.map( s => s._2)
    val bestWeekWeight = WeeksWeights.maxBy(_._2)
    println("---------------------")

    val ListBestWeeks = WeeksWeights.filter( week => week._2 >= 0)
    val listBestWeeksTratada = ListBestWeeks.map( week => week._1)
    return listBestWeeksTratada

  def geraHorario(scheduleInfo: ScheduleInfo) : Result[List[ScheduleDay]] =

    //Obtem diferentes combinações de dias possiveis
    val listDaysWeight = geraCombinacoesDia(scheduleInfo)

    val result = listDaysWeight.fold(left => Left(left),

      //Obtem diferentes semanas validas com base nos dias obtidos anteriormente
      listDaysWeight => obtainValidWeeks(listDaysWeight,scheduleInfo.constraint.minRestDaysPerWeek,scheduleInfo.ListNurses.toList)
        .fold(
          left => {
            println("left("+left)
            Left(left)
          },
          right => {
            //Caso encontre semanas validas
            println("right("+right)

            //Lista de Melhores Semanas
            val ListBestWeeks = getBestWeekByScheduleDayWeight(right)

            // Para cada elemento de lista de Melhores semanas aplicar o WeekOrganized
            val ListWeekOrganized = ListBestWeeks.map( week => {
              organizeWeekAccordingWithDayPreferences(scheduleInfo.preferences.listDayPreferences.toList,week)
            })

            //Obter Semana Organizada mais pesada
            val testpesos = ListWeekOrganized.map(week => week._2)
            val BestWeekOrganized = ListWeekOrganized.maxBy(_._2)
            val bestWeek = BestWeekOrganized._1

            //Ordenar dias da semana mais pesada
            val weekOrganizedSortedByDay = bestWeek.sortWith((s1,s2) => s1._2.to < s2._2.to).map(sd => sd._1)

            Right(weekOrganizedSortedByDay)
          }))

    return result


  /**
   * Obtem combinação de enfermeiros para um respetivo Nurse Requirement
   */
  def nurseCombinationForRequirement(ln: List[Nurse])(r: NurseRequirement): List[List[(Nurse,Role)]] =
    val lnf = ln.filter(_.listRoles.contains(r.role))
    lnf.map(n => (n,r.role)).combinations(r.number.to).toList

  /**
   * Combina Combinações para uma RoleRequirement com outra RoleRequirement
   *
   * Impede existencia de mais que um Nurse num Periodo
   */
  def combineSets(ll: List[List[(Nurse,Role)]], lncr: List[List[(Nurse,Role)]]): List[List[(Nurse,Role)]] =
    for
      l <- ll
      ncr <- lncr
      if(l.forall( (n,_) =>  !ncr.exists( (na,_) => n == na)))
    yield l ++ ncr

  /**
   * Metodo recursivo para obter diversos nurserequirements para um periodo
   */
  def nurseCombinationForRequirements(ln: List[Nurse], lr: List[NurseRequirement], ll: List[List[(Nurse,Role)]]) : List[List[(Nurse,Role)]] =
    if(lr.isEmpty) ll else
      val lncr = nurseCombinationForRequirement(ln)(lr.head)
      val nll = combineSets(ll,lncr)
      nurseCombinationForRequirements(ln,lr.tail,nll)


  /***
   * Combinações possiveis de periodos para um dia
   *
   * Sem invalidar constraint de maxShiftsPerDay
   */
  def combinePeriodosSets(llshifts: List[List[(ValidPeriod,List[(Nurse,Role)])]], combPeriodo: List[(ValidPeriod,List[(Nurse,Role)])], constraints: Constraint): List[List[(ValidPeriod,List[(Nurse,Role)])]] =
    for
      seqPeriodo <- llshifts
      shift <- combPeriodo

      tryCombinePeriods = seqPeriodo :+ shift
      if({
        val list = tryCombinePeriods.map( shift => shift._2.map( (n,_) => n._1)).flatten.groupBy(identity).map(n => (n._1, n._2.length))
        list.forall(_._2 <= constraints.maxShiftsPerDay.to)
      })
    yield seqPeriodo :+ shift


  /**
   *  Gera possiveis combinações de dias
   */
  def nurseCombinationForPeriods(ln: List[Nurse], lperiodos: List[SchedulePeriod], llshifts : List[List[(ValidPeriod,List[(Nurse,Role)])]], constraints: Constraint) : List[List[(ValidPeriod,List[(Nurse,Role)])]] =
    if(lperiodos.isEmpty) llshifts else
      val lshifts1 = nurseCombinationForRequirements(ln,lperiodos.head.listNurseRequirement.toList,List(List()))
      val lshifts2 = lshifts1.map( s => (lperiodos.head.id,s))
      val llshiftsCombined = combinePeriodosSets(llshifts,lshifts2,constraints)
      nurseCombinationForPeriods(ln,lperiodos.tail,llshiftsCombined,constraints)


  /**
   * A partir das combinações de dias possiveis, gera o objetivo ScheduleDay para cada um
   */
  def geraCombinacoesDia(info: ScheduleInfo): Result[List[ScheduleDay]] =
    val combinationsDay = nurseCombinationForPeriods(info.ListNurses.toList,info.listSchedulePeriod.toList,List(List()), info.constraint)
    val seqWeightedDays = combinationsDay.map( ll => {
      ScheduleDay.from(ll,info.preferences)
    })
    for
      listScheduleDay <- convert(seqWeightedDays)
      listDaysOrderedByWeight = listScheduleDay.sortWith(_.weight > _.weight)
    yield listDaysOrderedByWeight.toList



  //Map with Constraints to 7-ConstraintNumber = Work Days for each nurse
  //Gera mapa de todos os enfermeiros com os dias possiveis de trabalho
  def getMapWithListNurseAndWorkDays( listNursesToValidate: List[Nurse], constraint: ConstraintNumber) : Map[Nurse, Int] =
    listNursesToValidate.map( n => (n, 7-constraint.to)).toMap

  /**
   * Retorna mapa atualizado com os enfermeiros que estão a trabalhar e a lista de enfermeiros que já nao podem trabalhar mais
   */
  def validateMapWithListNurse(listNursesToValidate : List[Nurse], mapXPTO : Map[Nurse, Int]) : (List[Nurse] , Map[Nurse, Int]) =

    //Update Mapa
    val newMap : Map[Nurse, Int] = mapXPTO.map( (n,WorkDaysLeft) => if( listNursesToValidate.contains(n) ) (n,WorkDaysLeft-1) else (n,WorkDaysLeft) ).toMap

    //Obtem os enfermeiros q passaram dos limites
    val listNurses = newMap.foldRight[List[Nurse]](List()) ( (m,listNurses) => if( m._2 == 0 ) listNurses :+ m._1 else listNurses )

    (listNurses,newMap)


  //Retira da lista de dias recebido, todos os que contenham algum dos enfermeiros existentes na lista
  def retirarDaListDias(listDays: List[List[Nurse]] , listNurses: List[Nurse]) : List[List[Nurse]] =
    listDays.filter( day => day.forall( n => !listNurses.contains(n)))

  //Metodo auxiliar que permite obter apenas os restantes da lista
  def obterRestantesLista(day : List[Nurse], list: List[List[Nurse]]) : List[List[Nurse]] =
    if(day == list.head) list.tail else
      obterRestantesLista(day, list.tail)

  /**
   *   Metodo recursivo para obter lista de semanas possiveis
   *
   * Este metodo trabalha com List[Nurse] em vez de ScheduleDay, permitindo assim ter menos variedade de Possibilidades
   *
   */
  def obtainValidWeek(listOrderedDays: List[List[Nurse]], mapWithWorkDays: Map[Nurse, Int], validweek: List[List[Nurse]]) : Result[List[List[List[Nurse]]]] =

    //Valida se semana tem 7 dias, logo é valida
    if(validweek.length == 7) Right(List(validweek)) else

      //Caso a lista de dias restantes estiver vazia é sinal que nao é possivel criar semana
      if(listOrderedDays.isEmpty) return Left(NotEnoughNurses)

      val semanas = listOrderedDays.foldLeft[List[List[List[Nurse]]]](List())((semanas,day) => {

        //Valida mapa com lista de enfermeiros
        val mapvalidado = validateMapWithListNurse(day,mapWithWorkDays)

        if(mapvalidado._1.isEmpty) then
          //Valido ln
          val listSemAnteriores = day +: obterRestantesLista(day,listOrderedDays)

          val semana = obtainValidWeek(listSemAnteriores,mapvalidado._2,validweek :+ day)
          semana.fold(left => semanas, semana => semanas :+ semana.head)
        else
          //Significa que houve enfermeiros a ficar com 0 dias disponiveis de trabalho, por isso é necessário remover os dias
          // Que possam conter esses enfermeiros, reduzindo possiveis combinações
          val listDiasRestante : List[List[Nurse]] = day +: obterRestantesLista(day,listOrderedDays)
          val restantesDaListFiltered = retirarDaListDias(listDiasRestante, mapvalidado._1) //Obter dias que nao têm nurses que ja estao ocupados
          val semana = obtainValidWeek(restantesDaListFiltered,mapvalidado._2,validweek:+ day)
          semana.fold(left => semanas, semana => semanas :+ semana.head)


      })

      if(!semanas.isEmpty) Right(semanas) else Left(NotEnoughNurses)

  /**
   * Metodo auxiliar para obter o dia mais pesado que contem a lista de enfermeiros
   */
  def obtemDiasByNurses(listOrderedDays: List[ScheduleDay], listWeeks: List[List[List[Nurse]]]) : List[List[ScheduleDay]] =

    val mapa = listOrderedDays.groupBy(s => s.listNurses.sortBy(n => n.name.to)).map(t => (t._1,t._2.maxBy(_.weight)))

    val result = listWeeks.map( d => {
      d.map( ln => {
        mapa.get(ln) match {
          case Some(x) => x
          case None    => ScheduleDay(List(),List(),0)
        }
      })
    })
    return result
  

  def obtainValidWeeks(listOrderedDays: List[ScheduleDay], constraint: ConstraintNumber, listNurses: List[Nurse]) : Result[List[List[ScheduleDay]]] =

    //Gera mapa com enfermeiros e os dias de trabalho
    val  mapWithWorkDays: Map[Nurse, Int] = getMapWithListNurseAndWorkDays(listNurses ,constraint)

    //Obtem Dias transformados em lista de enfermeiros
    val listDayNurse = listOrderedDays.map(d => d.listNurses.sortBy(n => n.name.to)).distinct

    //Gera lista com semanas possiveis
    obtainValidWeek(listDayNurse, mapWithWorkDays,List()).fold(
      left => Left(left),
      right => Right(obtemDiasByNurses(listOrderedDays, right))
    )



  def genMapPrefsPerScheduleDay(preferences: List[DayPref], sd: ScheduleDay) : List[(Int, Int)] =
    val lista = List(1,2,3,4,5,6,7)
    val x = lista.map(f => {
      val somaPref = preferences.filter(dp => dp.day.to == f && sd.listNurses.contains(dp.nurse)).foldLeft[Int](0)((i,dp) => i + (dp.nurse.seniority.to * dp.value.to ))
      val totalSoma = (10*somaPref) + sd.weight
      (f,totalSoma)
    })
    return x


  def genMapForWeek(preferences: List[DayPref], week: List[ScheduleDay]) : List[(ScheduleDay, List[(Int, Int)])] =
    week.map(sd => {
      val prefsPerScheduleDay = genMapPrefsPerScheduleDay(preferences, sd)
      (sd, prefsPerScheduleDay)
    })


  def findDistintDaysPerWeight (list : List[(ScheduleDay, (Int, Int))]) : List[(ScheduleDay, (Int, Int))] =
    list.foldLeft[ List[(ScheduleDay, (Int, Int))]](List())((newList, sd) => {
      val objetoEncontrado = newList.filter(tup => tup._2._1 == sd._2._1)
      if(objetoEncontrado.isEmpty) newList :+ sd else {
        if(sd._2._2 > objetoEncontrado.head._2._2) then {
          val posDrop = newList.indexOf(objetoEncontrado)
          newList.drop(posDrop) :+ sd
        } else {
          newList
        }
      }
    })

  def organizeWeekAccordingWithDayPreferences(preferences: List[DayPref], week: List[ScheduleDay]) : (List[(ScheduleDay, Day)],Int) =
    val mapForWeek = genMapForWeek(preferences,week)
    val semana = organizeWeek(List(), mapForWeek)
    semana.foldLeft[(List[(ScheduleDay, Day)],Int)]((List(),0))( (total,sd) => {
      val semana = total._1 :+ (sd._1 , sd._2)
      val soma = total._2 + sd._3
      (semana,soma)
    })

  def updateMap( listDays : List[Int], mapUpdated : List[(ScheduleDay, List[(Int, Int)])]) : List[(ScheduleDay, List[(Int, Int)])] =
    mapUpdated.map(f => (f._1, f._2.filter(g => !listDays.contains(g._1))))

  def remove(listIndex: List[Int], list: List[(ScheduleDay, List[(Int, Int)])], index: Int, listUpdated : List[(ScheduleDay, List[(Int, Int)])]) : List[(ScheduleDay, List[(Int, Int)])] =
    if(list.isEmpty) listUpdated else
      if(listIndex.isEmpty || listIndex.head != index)
      then
        val dia = list.head
        remove(listIndex, list.tail, index+1,listUpdated :+ dia)
      else
        remove(listIndex.tail, list.tail, index+1,listUpdated)



  def organizeWeek(semana : List[(ScheduleDay, Day,Int)],  mapUpdated : List[(ScheduleDay, List[(Int, Int)])])  : List[(ScheduleDay, Day,Int)] =
    if(semana.length == 7) semana else
      val t :  List[(ScheduleDay, (Int, Int))] = mapUpdated.map(f => (f._1,f._2.maxBy(_._2)))
      val list = findDistintDaysPerWeight(t)
      val semanaAtualizada = list.foldLeft(semana)((semana, newDay) => {
        Day.from(newDay._2._1.toString) match {
          case Right(x) => semana :+ (newDay._1, x, newDay._2._2)
        }
      })
      val listdias = list.map(t => t._2._1)
      //val mapUpdated2 = updateMap(listdias, mapUpdated)
      val mapTest = mapUpdated.map(sd => sd._1)
      val listScheduleIndexToDrop = list.map( sd => mapTest.indexOf( sd._1)).sortWith((i1,i2) => i1<i2)
      //val mapUpdated3 = listScheduleIndexToDrop.foldLeft[List[(ScheduleDay, List[(Int, Int)])]](mapUpdated)((map,i) => remove(i,map))
      val mapUpdated3 = remove(listScheduleIndexToDrop, mapUpdated,0,List())
      val mapUpdated2 = updateMap(listdias, mapUpdated3)
      organizeWeek(semanaAtualizada, mapUpdated2)



  def exportToXML(seqSeqShift: List[ScheduleDay]): Elem =
    <shiftSchedule xmlns="http://www.dei.isep.ipp.pt/tap-2022" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.dei.isep.ipp.pt/tap-2022 ../../schedule.xsd ">
      {(1 to seqSeqShift.size).map(i => exportXML2(seqSeqShift(i-1).listShifts,i))}
    </shiftSchedule>

  def exportXML2(SeqShift: List[Shift], i: Int): Elem =
    <day id={i.toString}>
      {SeqShift.map(Elem => Shift.exportToXML(Elem))}
    </day>