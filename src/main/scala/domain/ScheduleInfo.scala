package domain

import xml.XML.{traverse}
import domain.*
import scala.xml.Node
import domain.SimpleTypes.*
import domain.DomainError.*

final case class ScheduleInfo(listSchedulePeriod: Seq[SchedulePeriod], ListNurses : Seq[Nurse], preferences : Preferences, constraint : Constraint)

object ScheduleInfo:

  //Atributos do ScheduleInfo
  val schedulePeriodNode: String = "schedulingPeriod"
  val PreferenceNode: String = "preferences"
  val NurseNode: String = "nurse"
  val ConstraintNode: String = "constraints"

  def from(xml: Node): Result[ScheduleInfo] =
    for{
      lsp <- traverse((xml \\ schedulePeriodNode), (n:Node) => SchedulePeriod.from(n))
      lnurses <- traverse((xml \\ NurseNode), (n:Node) => Nurse.from(n))
      //validar numero de requirements necessários de uma role com os enfermeiros existentes
      resultValidation <- validateListNurses(lsp,lnurses)
      preferences <- traverse((xml \\ PreferenceNode), (n:Node) => Preferences.from(n,lsp, lnurses))
      constraint <- traverse((xml \\ ConstraintNode), (n:Node) => Constraint.from(n))
    } yield ScheduleInfo(lsp, lnurses, preferences.head, constraint.head)

  def validateListNurses(listPeriods: Seq[SchedulePeriod], listNurses: Seq[Nurse]): Result[Unit] =

    // Map[("A" -> 1), ("B" -> 2)]
    val CounterNurseRoles = listNurses.map(_.listRoles).flatten.groupBy(x => x).map(t=> (t._1, t._2.length))
    val listPeriodsFlatten = listPeriods.map(_.listNurseRequirement).flatten

    //Tries to find the first nurserequirement that doesnt have enough nurses
    for {
      ContainsRole <- listPeriodsFlatten.find(nr => !(CounterNurseRoles contains nr.role) ).fold(Right(true))(nr => Left(UnknownRequirementRole(nr.role.to)))
      result <- listPeriodsFlatten.find(nr => nr.number.to > CounterNurseRoles(nr.role)).fold(Right(true))(nr => Left(NotEnoughNurses))
    } yield (ContainsRole,result)



