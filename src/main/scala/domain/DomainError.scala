package domain

type Result[A] = Either[DomainError,A]

enum DomainError:
  case IOFileProblem(error: String)
  case XMLError(error: String)
  case PreferenceValueOutOfRange(error: String)
  case InvalidPreferenceValueFormat(error: String)
  case InvalidDayOutOfRange(error: String)
  case InvalidDayFormat(error: String)
  case UnknownPeriod(error: String)
  case UnknownNurse(error: String)
  case SeniorityValueOutOfRange(error: String)
  case InvalidSeniorityValueFormat(error: String)
  case InvalidEmptyName(error: String)
  case InvalidNurseRequirementNumberOutOfRange(error: String)
  case InvalidNurseRequirementNumberFormat(error: String)
  case InvalidUnkownRole(error: String)
  case InvalidPeriod(error: String)
  case InvalidTimeFormat(error: String)
  case InvalidConstraintNumberOutOfRange(error: String)
  case InvalidConstraintNumberFormat(error: String)
  case NotEnoughNurses
  case UnknownRequirementRole(error: String)
  case EmptyName

