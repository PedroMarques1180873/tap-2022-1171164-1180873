package domain

import domain.SimpleTypes.ValidPeriod
import domain.SimpleTypes.Role

import scala.xml.Elem

final case class Shift(id: ValidPeriod, seqNurseRole: Seq[(Nurse,Role)], weight: Int)

object Shift :

  def from(id: String, seq : Seq[(Nurse,Role)], preferences: Seq[PeriodPref]): Result[Shift] =
    for {
      idAttr <- ValidPeriod.from(id)
      weight = calculateWeightShift(idAttr,seq,preferences)
    } yield Shift(idAttr, seq,weight)



  def calculateWeightShift(period : ValidPeriod, seq: Seq[(Nurse,Role)],preferences:Seq[PeriodPref]): Int =
    seq.foldLeft[Int](0)( (total,n) => {

      val listPrefFoundPeriod = preferences.filter( periodpref => periodpref.period.id == period)
      if(listPrefFoundPeriod.isEmpty) total else
        listPrefFoundPeriod.find( periodPref => periodPref.nurse == n._1) match {
          case None => total
          case Some(periodPref) => total + (periodPref.value.to * n._1.seniority.to)
        }
    })

  def exportToXML(shift : Shift): Elem =
    <shift id={shift.id.toString}>
      {shift.seqNurseRole.sortBy(_._1.name.to).map(Elem => Nurse.exportToXML(Elem))}
    </shift>