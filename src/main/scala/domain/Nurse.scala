package domain

import scala.xml.{Elem, Node}
import xml.XML.traverse
import xml.XML.fromAttribute
import domain.SimpleTypes.*
import domain.DomainError.*

import scala.annotation.targetName

final case class Nurse(name: NurseName, seniority: NurseSeniority, listRoles: Seq[Role])

object Nurse :

  val nameAttribute: String = "name"
  val seniorityAttribute: String = "seniority"
  val RolesNode: String = "nurseRole"

  def from(xml: Node): Result[Nurse] =
    for {
      name <- fromAttribute(xml, nameAttribute).fold[Result[NurseName]](left => Left(EmptyName), StringValue => NurseName.from(StringValue))
      seniority <- fromAttribute(xml, seniorityAttribute).fold[Result[NurseSeniority]](left => Left(left), StringValue => NurseSeniority.from(StringValue))
      stringListRoles <- traverse((xml \\ RolesNode), (n:Node) => fromAttribute(n, "role").fold[Result[Role]](left => Left(left), StringValue => Role.from(StringValue)))
    } yield Nurse(name, seniority, stringListRoles)

  extension(nurse: Nurse)
    @targetName("toNurseName")
    def to: String = nurse.name.to
  
  def exportToXML(nurse: (Nurse,Role)): Elem =
    <nurse name={nurse._1.name.to} role={nurse._2.to}/>