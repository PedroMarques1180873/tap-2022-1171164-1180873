package domain

import scala.xml.Node
import xml.XML.traverse
import xml.XML.fromAttribute
import domain.DomainError.*
import domain.SimpleTypes.*


final case class DayPref(nurse: Nurse, day: Day, value: PreferenceValue)

object DayPref:

  val valueAttribute: String = "value"
  val nurseAttribute: String = "nurse"
  val dayAttribute: String = "day"

  def obtainNurse(listNurses: Seq[Nurse], nurseRead : String) : Result[Nurse]=
    for {
      result <- listNurses.find(x => x.to == nurseRead).fold(Left(UnknownNurse(nurseRead)))( x => Right(x))
    } yield result

  def from(xml : Node, listNurses: Seq[Nurse]) : Result[DayPref] =
    for {
      nurseRead <- fromAttribute(xml, nurseAttribute)
      nurse <- obtainNurse(listNurses, nurseRead)
      day <- fromAttribute(xml, dayAttribute).fold[Result[Day]](left => Left(left), StringDay => Day.from(StringDay))
      value <- fromAttribute(xml, valueAttribute).fold[Result[PreferenceValue]](left => Left(left), StringValue => PreferenceValue.from(StringValue))
    } yield DayPref(nurse, day, value)

  