package domain

import domain.SimpleTypes.*
import xml.XML.{fromAttribute, traverse}

import scala.xml.Node

final case class NurseRequirement(number: NurseRequirementNumber, role: Role)

object NurseRequirement:

  val numberAttribute: String = "number"
  val roleAttribute: String = "role"

  def from(xml: Node): Result[NurseRequirement] =
    for {
      number <- fromAttribute(xml, numberAttribute).fold[Result[NurseRequirementNumber]](left => Left(left), StringValue => NurseRequirementNumber.from(StringValue))
      role <- fromAttribute(xml, roleAttribute).fold[Result[Role]](left => Left(left), StringValue => Role.from(StringValue))
    } yield NurseRequirement(number, role)



